use dbms_final;

insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('李相赫', 'Faker', 'SKT', '奧莉安娜', '卡莎碧雅', '犽宿');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('金鐘仁', 'PraY', 'LZ', '伊澤瑞爾', '凱特琳', '路西恩');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('姜範慧', 'GorillA', 'LZ', '瑟雷西', '布里茨', '魔甘娜');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('金東華', 'Khan', 'LZ', '菲歐拉', '卡蜜兒', '雷尼克頓');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('李元浩', 'Xiaohu', 'RNG', '艾克', '逆命', '飛斯');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('郭博成', 'Bdd', 'LZ', '塔莉雅', '飛斯', '犽宿');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('李宰完', 'Wolf', 'SKT', '索娜', '瑟雷西', '布里茨');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('李賢真', 'CuVee', 'SSG', '嘉文四世', '達瑞斯', '弗拉迪米爾');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('田野', 'Meiko', 'EDG', '卡瑪', '布郎姆', '納帝魯斯');
insert into esport_player(playername, playergamename, playerteam, playermostuse1, playermostuse2, playermostuse3)
values ('Søren Bjerg', 'Bjergsen', 'TSM', '拉克絲', '逆命', '維克特');
