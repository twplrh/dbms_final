use dbms_final;

create table if not exists Heros
(
	HeroName varchar(20),
    HeroTitle varchar(20),
    HeroUsedRate float,
    HeroBanRate float,
	primary key (HeroName)
);

create table if not exists Esport_player
(
	ID INT auto_increment,
	playername varchar(20),
    playergamename varchar(20),
    playerteam varchar(20),
    playermostuse1 varchar(20),
    playermostuse2 varchar(20),
    playermostuse3 varchar(20),
    primary key (id)
);

create table if not exists Esport_team
(
	ID int auto_increment,
	teamname varchar(20),
    teamleader varchar(20),
    teamcountry varchar(20),
    teamfounddate varchar(20),
    primary key (id)
);

create table if not exists GameRecord
(
	ID int auto_increment,
	GameTime varchar(20),
    GameName varchar(20),
    GameTeam1 varchar(20),
    GameTeam2 varchar(20),
    GameTeam1Score varchar(20),
    GameTeam2Score varchar(20),
    GameAnchor varchar(20),
	primary key (id)
);

create table if not exists Gameanchor
(
	ID int auto_increment,
    AnchorName varchar(20),
    AnchorFame varchar(20),
    AnchorHistory1 varchar(20),
    AnchorHistory2 varchar(20),
    primary key (id)
);

