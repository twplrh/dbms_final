use dbms_final;

insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '八強淘汰賽01', 'RNG', 'FNC', '3', '1', '叉燒');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '八強淘汰賽02', 'SKT', 'MSF', '3', '2', '叉燒');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '八強淘汰賽03', 'WE', 'C9', '3', '2', 'Egg');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '八強淘汰賽04', 'LZ', 'SSG', '0', '3', '烏龍');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '四強準決賽01', 'SKT', 'RNG', '3', '2', '烏龍');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '四強準決賽02', 'SSG', 'WE', '3', '1', '波波尼');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2017', '冠亞總決賽01', 'SKT', 'SSG', '0', '3', 'Lilballz');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2018', 'LMS升降賽01', 'FW', 'G-Rex', '3', '0', 'Woody');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2018', 'LMS升降賽02', 'Machi', 'Mad', '0', '3', 'Egg');
insert into gamerecord(	GameTime,GameName ,GameTeam1 ,GameTeam2 ,GameTeam1Score ,GameTeam2Score, GameAnchor) 
values ('2018', 'LMS升降賽03', 'ahq', 'Jteam', '2', '0', '貝克');