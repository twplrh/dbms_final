use dbms_final;

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('伊瑞莉雅', '舞劍靈使', 0.47, 0.11);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate) 
values ('李星', '盲眼武僧', 0.37, 0.07);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('索娜', '琴仙', 0.35, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate) 
values ('星朵拉', '黑暗領主', 0.36, 0.06);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate) 
values ('法洛士', '懲戒之箭', 0.36, 0.04);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('阿祈爾', '蘇瑞瑪砂皇', 0.43, 0.08);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('塔莉雅', '岩石編織者', 0.49, 0.12);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('雷尼克頓', '沙漠屠夫', 0.29, 0.01);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('納瑟斯', '沙漠死神', 0.31, 0.01);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('阿姆姆', '殤之木乃伊', 0.24, 0.01);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('凱特琳', '執法者', 0.47, 0.03);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('奧莉安娜', '發條少女', 0.47, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('伊澤瑞爾', '探險家', 0.47, 0.17);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('杰西', '明日守護者', 0.30, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡蜜兒', '鋼鐵殘影', 0.45, 0.09);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('艾希', '冰霜射手', 0.38, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('史瓦妮', '北境之怒', 0.41, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('麗珊卓', '傲雪巫女', 0.34, 0.01);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('艾妮維亞', '冰晶鳳凰', 0.29, 0.00);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('弗力貝爾', '雷霆熊吼', 0.23, 0.00);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡力斯', '虛空掠食者', 0.51, 0.12);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('寇格魔', '深淵巨口', 0.36, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡薩丁', '虛空行者', 0.41, 0.07);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('雷珂煞', '虛空穿梭者', 0.41, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('科加斯', '虛空恐懼', 0.33, 0.01);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('派克', '溺流鬼影', 0.61, 0.17);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('飛斯', '深海頑童', 0.53, 0.12);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('葛雷夫', '亡命之徒', 0.51, 0.16);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('逆命', '卡牌大師', 0.52, 0.13);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('好運姊', '賞金獵人', 0.44, 0.07);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡特蓮娜','不祥之刃', 0.47, 0.07);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('魔甘娜', '墮落天使', 0.47, 0.03);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('弗拉迪米爾', '血色收割者', 0.51, 0.15);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('達瑞文', '處刑娛樂大師', 0.48, 0.15);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('達瑞斯', '諾克薩斯之血', 0.45, 0.12);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('菲歐拉','孤高劍客', 0.44, 0.08);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('拉克絲','光之少女', 0.39, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('加里歐', '禦國巨像', 0.42, 0.03);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('路西恩','驅魔聖槍', 0.29, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('嘉文四世','蒂瑪西亞楷模', 0.37, 0.06);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('吉茵珂絲','暴走重砲', 0.46, 0.10);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('菲艾', '鐵腕特警', 0.43, 0.08);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('艾克', '時空少年', 0.41, 0.07);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('布里茨', '蒸汽巨神兵', 0.41, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('維克特', '機械使徒', 0.19, 0.00);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('伊莉絲', '蜘蛛女王', 0.42, 0.03);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('伊芙琳', '臨終擁抱', 0.44, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('赫克林', '戰爭之影', 0.38, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('克黎思妲', '復仇之矛', 0.37, 0.06);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡爾瑟斯', '死亡頌唱者', 0.34, 0.00);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('茂凱', '扭曲樹人', 0.28, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('魔鬥凱薩', '金屬亡靈', 0.24, 0.01);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('瑟雷西', '鍊魂獄長', 0.48, 0.04);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('約瑞科', '亡魂牧人', 0.39, 0.03);

insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡莎碧雅', '蛇之擁抱', 0.41, 0.02);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('犽宿', '放逐浪人', 0.47, 0.07);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('卡瑪', '魂之啟者', 0.48, 0.12);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('珍娜', '風暴女神', 0.40, 0.03);
insert into Heros(HeroName, HeroTitle, HeroUsedRate, HeroBanRate)
values ('納帝魯斯', '深淵巨人', 0.31, 0.03);

insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('叉燒', 'bad', '前閃電狼教練', 'TPA戰術分析師');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('EGG', 'good', '校際盃的播報', 'MSI播報');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('烏龍', 'normal', 'Twitch實況主', 'MSI播報');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('波波尼', 'normal', '過氣珍娜神', '前閃電狼輔助');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('貝克', 'good', '前AHQ教練', 'GPL 聯賽賽評');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('Lilballz', 'good', '鬥陣特攻主播', '爐石主播');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('Woody', 'good', '網咖爭霸賽播報', '爐石選手');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('JWELL', 'good', 'Twitch實況主', 'TPA戰術分析師');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('RORO ', 'normal', 'Twitch實況主', '校際盃主播');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('KH', 'bad', 'MSI播報', '英雄聯盟主播');
