use dbms_final;

insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('叉燒', 'bad', '前閃電狼教練', 'TPA戰術分析師');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('EGG', 'good', '校際盃的播報', 'MSI播報');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('烏龍', 'normal', 'Twitch實況主', 'MSI播報');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('波波尼', 'normal', '過氣珍娜神', '前閃電狼輔助');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('貝克', 'good', '前AHQ教練', 'GPL 聯賽賽評');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('Lilballz', 'good', '鬥陣特攻主播', '爐石主播');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('Woody', 'good', '網咖爭霸賽播報', '爐石選手');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('JWELL', 'good', 'Twitch實況主', 'TPA戰術分析師');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('RORO ', 'normal', 'Twitch實況主', '校際盃主播');
insert into Gameanchor(	AnchorName, AnchorFame , AnchorHistory1, AnchorHistory2) 
values ('KH', 'bad', 'MSI播報', '英雄聯盟主播');