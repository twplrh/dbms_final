use dbms_final;

insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('SKT', 'Faker', 'Korea', '2013');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('SSG', 'Mata', 'Korea', '2015');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('LZ', 'Gorilla', 'Korea', '2016');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('EDG', 'Nofe', 'China', '2013');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('RNG', 'Uzi', 'China', '2016');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('TSM', 'Bjergsen', 'NorthAmerica', '2009');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('FW', 'Maple', 'Taiwan', '2013');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('AHQ', 'NeXAbc', 'Taiwan', '2012');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('FNC', 'Xizt', 'Europe', '2004');
insert into esport_team (teamname, teamleader, teamcountry, teamfounddate) 
values ('G2', 'PerkZ', 'Europe', '2014');